import { showModal } from './modal'
import { createElement } from '../../helpers/domHelper'


export function showWinnerModal(fighter) {
  const body = createElement({
    tagName: 'img',
    attributes: {
      src: fighter.source,
      alt: fighter.name
    }
  })
  showModal({title:`${fighter.name} WINS!`, bodyElement: body})
}
