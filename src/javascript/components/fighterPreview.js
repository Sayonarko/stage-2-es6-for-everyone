import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  // todo: show fighter info (image, name, health, etc.)
  if (fighter) {
    const { name, health, defense, attack } = fighter
    const fighterImage = createFighterImage(fighter)
    const fighterInfo = createElement({ tagName: 'div', className: 'fighter-preview__info' });
    fighterInfo.innerHTML = `
      <h3>${name}</h3>
      <div>Health: <span>${health}</span></div>
      <div>Attack: <span>${attack}</span></div>
      <div>Defense: <span>${defense}</span></div>
    `
    fighterElement.append(fighterImage, fighterInfo);

  }

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
