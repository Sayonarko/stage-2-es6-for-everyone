import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {

  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over
    const firstHealthBar = document.getElementById("left-fighter-indicator")
    const secondHealthBar = document.getElementById("right-fighter-indicator")
    const {
      PlayerOneAttack,
      PlayerOneBlock,
      PlayerTwoAttack,
      PlayerTwoBlock,
      PlayerOneCriticalHitCombination,
      PlayerTwoCriticalHitCombination
    } = controls
    const keysDown = {
      keyA: false,
      keyD: false,
      keyQ: false,
      keyW: false,
      keyE: false,
      keyJ: false,
      keyL: false,
      keyU: false,
      keyI: false,
      keyO: false
    }
    const coolDowns = {
      firstFighter: false,
      secondFighter: false
    }

    document.addEventListener("keydown", e => {

      switch (e.code) {
        //player one attack
        case PlayerOneAttack: {
          keysDown.keyA = true
          if (keysDown.keyA && !keysDown.keyD && !keysDown.keyL) {
            const hit = getDamage(firstFighter, secondFighter)
            changeHealth(secondFighter, hit, secondHealthBar)
            keysDown.keyA = false
          }
        }
          break
        //player one block
        case PlayerOneBlock:
          keysDown.keyD = true
          break
        //player two attack
        case PlayerTwoAttack: {
          keysDown.keyJ = true
          const hit = getDamage(secondFighter, firstFighter)
          if (keysDown.keyJ && !keysDown.keyD && !keysDown.keyL) {
            changeHealth(firstFighter, hit, firstHealthBar)
            keysDown.keyJ = false
          }
        }
          break
        //player two block
        case PlayerTwoBlock:
          keysDown.keyL = true
          break
        //player one critical
        case PlayerOneCriticalHitCombination[0]:
          keysDown.keyQ = true
          firstFighterCriticalHit()
          break
        case PlayerOneCriticalHitCombination[1]:
          keysDown.keyW = true
          firstFighterCriticalHit()
          break
        case PlayerOneCriticalHitCombination[2]:
          keysDown.keyE = true
          firstFighterCriticalHit()
          break
        //player two critical
        case PlayerTwoCriticalHitCombination[0]:
          keysDown.keyU = true
          secondFighterCriticalHit()
          break
        case PlayerTwoCriticalHitCombination[1]:
          keysDown.keyI = true
          secondFighterCriticalHit()
          break
        case PlayerTwoCriticalHitCombination[2]:
          keysDown.keyO = true
          secondFighterCriticalHit()
          break
        default:
          return
      }
    })

    document.addEventListener("keyup", e => {
      switch (e.code) {
        case PlayerOneBlock:
          keysDown.keyD = false
          break
        case PlayerTwoBlock:
          keysDown.keyL = false
          break
        default:
          return
      }
    })

    function changeHealth(fighter, hit, element) {
      const healthBarWidth = element.offsetWidth
      const persentHit = hit / (fighter.health / 100)

      if (hit * persentHit > healthBarWidth) {
        element.style.width = 0
      }
      if (!firstHealthBar.offsetWidth) {
        resolve(secondFighter)
      }
      if (!secondHealthBar.offsetWidth) {
        resolve(firstFighter)
      }
      element.style.width = healthBarWidth - hit * persentHit + 'px'
    }

    function firstFighterCriticalHit() {
      if (keysDown.keyQ && keysDown.keyW && keysDown.keyE && !coolDowns.firstFighter) {
        const hit = getCritHitPower(firstFighter)
        changeHealth(secondFighter, hit, secondHealthBar)
        keysDown.keyQ,
        keysDown.keyW,
        keysDown.keyE = false
        coolDowns.firstFighter = true
        setTimeout(() => {
          coolDowns.firstFighter = false
        }, 10000)
      }
    }

    function secondFighterCriticalHit() {
      if (keysDown.keyU && keysDown.keyI && keysDown.keyO && !coolDowns.secondFighter) {
        const hit = getCritHitPower(secondFighter)
        changeHealth(firstFighter, hit, firstHealthBar)
        keysDown.keyU,
        keysDown.keyI,
        keysDown.keyO = false
        coolDowns.secondFighter = true
        setTimeout(() => {
          coolDowns.secondFighter = false
        }, 10000)
      }
    }
  });
}

export function getDamage(attacker, defender) {
  // return damage
  const damage = getHitPower(attacker) - getBlockPower(defender)
  return damage > 0 ? damage : 0
}

export function getHitPower(fighter) {
  // return hit power
  const criticalHitChance = Math.random() + 1
  const hitPower = fighter.attack * criticalHitChance
  return hitPower
}

export function getBlockPower(fighter) {
  // return block power
  const dodgeChance = Math.random() + 1
  const blockPower = fighter.defense * dodgeChance
  return blockPower
}

export function getCritHitPower(fighter) {
  // return critical power
  const critPower = fighter.attack * 2
  return critPower
}

